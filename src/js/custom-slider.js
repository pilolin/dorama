function css(element, styles) {
	for (const key in styles) {
		element.style[key] = styles[key];
	}
}

export default class CustomSlider {
	constructor (selector) {
		this.selector = selector;
		this.sliderElement = document.querySelector(selector);

		if( !this.sliderElement ) return;

		this.sliderWrapElement = this.sliderElement.querySelector('.custom-slider__wrapper');

		this.countSlides = this.sliderElement.querySelectorAll('.custom-slider__slide').length;

		this.containerWidth;
		this.containerHeight;
		this.activeSlideWidth;
		this.activeSlideHeight;
	}

	/*
		Клонируем все слайды чтобы минимум было 5 штук, для сглаживания переходов
		Слайдам расставляем индексы
		Раздаем классы и устанавливаем положение
		Добавляем нужных слушателей событий (кнопки)
	*/
	init() {
		if( !this.sliderElement ) return;
		
		let slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		for( const indexSlide in [...slides] ) {
			let clone = slides[indexSlide].cloneNode(true);

			slides[indexSlide].dataset.index = parseInt(indexSlide) + 1;

			clone.dataset.index = parseInt(indexSlide) + this.countSlides + 1;
			clone.classList.add('custom-slide-clone');
			this.sliderWrapElement.append(clone);
		}

		slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.sliderWrapElement.prepend(slides[slides.length - 1]);
		this.sliderWrapElement.prepend(slides[slides.length - 2]);

		this.setClasses();
		
		this.setPositionSlider();

		this.createButtonNav();

		window.addEventListener('resize', () => { this.setPositionSlider(); });

		const swipe = new Hammer(this.sliderElement);

		swipe.on('swipeleft swiperight', ev => {
			if(ev.type === 'swipeleft') {
				this.nextSlide();
			} else {
				this.prevSlide();
			}
		});
	}

	createButtonNav() {
		const prevButton = document.createElement('button');
		const nextButton = document.createElement('button');

		prevButton.classList.add('custom-slider__button-prev', 'custom-slider__button');
		prevButton.innerHTML = '<svg class="icon-svg-arrow-prev"><use xlink:href="#arrow-prev"></svg>';
		prevButton.addEventListener('click', () => { this.prevSlide(); });
		this.sliderElement.append(prevButton);

		nextButton.classList.add('custom-slider__button-next', 'custom-slider__button');
		nextButton.innerHTML = '<svg class="icon-svg-arrow-next"><use xlink:href="#arrow-next"></svg>';
		nextButton.addEventListener('click', () => { this.nextSlide(); });
		this.sliderElement.append(nextButton);
	}

	setPositionSlider() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.containerWidth = this.sliderElement.offsetWidth;

		if (window.matchMedia('(max-width: 769px)').matches) {
			this.containerHeight = 0.275 * this.containerWidth;
			this.activeSlideWidth = 0.406 * this.containerWidth;
			this.activeSlideHeight = 0.647 * this.activeSlideWidth;
		} else {
			this.containerHeight = 0.608 * this.containerWidth;
			this.activeSlideWidth = 0.777 * this.containerWidth;
			this.activeSlideHeight = 0.6723 * this.activeSlideWidth;
		}

		for( const indexSlide in [...slides] ) {
			css(slides[indexSlide], {
				width: this.activeSlideWidth + 'px',
				height: this.activeSlideHeight + 'px'
			});
		}

		css(this.sliderElement, { height: this.containerHeight + 'px' });

		this.setPositionSlides();
	}

	// установка классов активным слайдам
	setClasses() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		for( const indexSlide in [...slides] ) {
			slides[indexSlide].classList.remove('custom-slider__slide-prev', 'custom-slider__slide-next', 'custom-slider__slide-active');
		}

		this.sliderElement.querySelector(`.custom-slider__slide:nth-child(2)`).classList.add('custom-slider__slide-prev');
		this.sliderElement.querySelector(`.custom-slider__slide:nth-child(3)`).classList.add('custom-slider__slide-active');
		this.sliderElement.querySelector(`.custom-slider__slide:nth-child(4)`).classList.add('custom-slider__slide-next');
	}

	// установка положения видимых слайдов
	setPositionSlides() {
		if(window.matchMedia('(max-width: 769px)').matches) {
			this.setMobilePosition();
		} else {
			this.setDesktopPosition();
		}
	}

	// установка положения активным слайдам в мобильной версии
	setMobilePosition() {
		const prev2Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(1)`);
		const prev1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(2)`);
		const activeSlide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(3)`);
		const next1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(4)`);
		const next2Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(5)`);

		css(prev2Slide, {
			transform: 'translate3d(-' + (this.activeSlideWidth - 8) + 'px, 0, 0)',
			opacity: 0
		});
		
		css(prev1Slide, {
				transform: 'translate3d(0, 0, 0)',
				opacity: 1
		});

		css(activeSlide, {
				transform: 'translate3d(' + (this.activeSlideWidth + 8) + 'px, 0, 0)',
				opacity: 1
		});

		css(next1Slide, {
				transform: 'translate3d(' + (2 * (this.activeSlideWidth + 8)) + 'px, 0, 0)',
				opacity: 1
		});

		css(next2Slide, {
				transform: 'translate3d(' + (3 * (this.activeSlideWidth + 8)) + 'px, 0, 0)',
				opacity: 0
		});
	}

	// установка положения активным слайдам в десктоп версии
	setDesktopPosition() {
		const prev2Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(1)`);
		const prev1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(2)`);
		const activeSlide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(3)`);
		const next1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(4)`);
		const next2Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(5)`);

		css(prev2Slide, {
			transform: `translate3d(
										${(0.729 - 1) * 0.5 * this.activeSlideWidth}px, 
										${this.containerHeight - this.activeSlideHeight }px, 
										-150px
									) 
									scale(0.729)`,
			opacity: 0
		});

		css(prev1Slide, {
			transform: `translate3d(
										${(0.729 - 1) * 0.5 * this.activeSlideWidth}px, 
										${this.containerHeight - this.activeSlideHeight }px, 
										-100px
									) 
									scale(0.729)`,
			opacity: .5
		});

		css(activeSlide, {
			transform: `translate3d(
										${0.095 * this.containerWidth}px, 
										${this.containerHeight - this.activeSlideHeight}px, 
										0
									)`,
			opacity: 1
		});

		css(next1Slide, {
			transform: `translate3d(
										${this.containerWidth - this.activeSlideWidth + ((1 - 0.775) * 0.5 * this.activeSlideWidth)}px, 
										${(0.775 - 1) * 0.5 * this.activeSlideHeight}px, 
										-100px
									) 
									scale(0.775)`,
			opacity: .5
		});

		css(next2Slide, {
			transform: `translate3d(
										${this.containerWidth - this.activeSlideWidth + ((1 - 0.775) * 0.5 * this.activeSlideWidth)}px, 
										${(0.775 - 1) * 0.5 * this.activeSlideHeight}px, 
										-150px
									) 
									scale(0.775)`,
			opacity: 0
		});
	}

	// следующий слайд
	nextSlide() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.sliderWrapElement.append(slides[0])

		if(this.currentIndex + 1 > (2 * this.countSlides)) {
			this.currentIndex = 1;
		} else {
			this.currentIndex++;
		}

		this.setClasses();
		this.setPositionSlides();
	}

	// предыдущий слайд
	prevSlide() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.sliderWrapElement.prepend(slides[(2 * this.countSlides) - 1]);

		if(this.currentIndex - 1 < 1) {
			this.currentIndex = 2 * this.countSlides;
		} else {
			this.currentIndex--;
		}

		this.setClasses();
		this.setPositionSlides();
	}
}