import { hideScroll, showScroll } from './manageScrollDocument';
import { debounce } from './helper';

// открытие быстрого поиска в шапке
document.addEventListener('click', e => {
	if( !e.target.closest('.header .btn__search') ) return;

	const header = e.target.closest('.header');
	const headerSearchField = header.querySelector('.header-search-field');
	const input = headerSearchField.querySelector('input');
	const searchResultContent = header.querySelector('.simplebar-content');

	headerSearchField.style.opacity = 1;
	header.classList.add('search-opened');

	setHeightSearchResultContainer();
	window.addEventListener('resize', debounceSetHeightSearchResultContainer);

	input.focus();

	// фикс - на некоторых iphone резуьтат поиска скролится при открытии быстрого поиска
	searchResultContent.scrollIntoView();

	hideScroll();
});

// закрытие быстрого поиска в шапке
document.addEventListener('click', e => {
	if( e.target.closest('.header .header-search__button-close') || e.target.closest('.header-search__backdrop') ) {
		const header = document.querySelector('.header');
		const headerSearchField = header.querySelector('.header-search-field');
		const form = headerSearchField.querySelector('form');
	
		header.classList.remove('search-opened');
		setTimeout(() => {
			headerSearchField.style.opacity = 0;
		}, 250);

		window.removeEventListener('resize', debounceSetHeightSearchResultContainer);

		form.reset();
		
		showScroll();
	}
});

const debounceSetHeightSearchResultContainer = debounce(setHeightSearchResultContainer, 300);

function setHeightSearchResultContainer() {
	const searchResultContainer = document.querySelector('.header-search-result-wrap');
	const rectSearchResultContainer = searchResultContainer.getBoundingClientRect();
	let heightSearchResultContainer = 0;
	
	if(!device.mobile() && window.matchMedia('(min-width: 900px)').matches) {

		heightSearchResultContainer = window.innerHeight - 200;

		if(!window.matchMedia('(max-width: 1024px)').matches) {
			heightSearchResultContainer -= rectSearchResultContainer.top;
		}
		searchResultContainer.style.height = 'auto';
		searchResultContainer.style.maxHeight = heightSearchResultContainer + 'px';

	} else {

		heightSearchResultContainer = window.innerHeight - rectSearchResultContainer.top;
		searchResultContainer.style.height = heightSearchResultContainer + 'px';
		searchResultContainer.style.maxHeight = heightSearchResultContainer + 'px';

	}
}