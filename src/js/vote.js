import { isTouchDevice } from './helper';

// рейтинг
document.addEventListener('mouseover', function(e) {
	const target = e.target;

	if( target.closest('.rating__star') && !isTouchDevice()) {
		const ratingWrap = target.closest('.rating-wrap');
		const currentStar = target.closest('.rating__star');
		const listStar = ratingWrap.children;
		let hoverStar = true;

		for (const star of [...listStar]) {
			if(hoverStar) {
				star.classList.add('rating__star_hover');
			} else {
				star.classList.remove('rating__star_hover');
			}

			if( hoverStar && star === currentStar ) {
				hoverStar = false;
			}
		}
	}
});

document.addEventListener('mouseout', function(e) {
	const target = e.target;

	if( target.closest('.rating') && !isTouchDevice()) {
		const ratingWrap = target.closest('.rating-wrap');

		if( ratingWrap ) {
			const listStar = ratingWrap.children;
	
			for (const i in [...listStar]) {
				const star = listStar[i];

				star.classList.remove('rating__star_hover');
			}
		}
	}
});