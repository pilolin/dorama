import { hideScroll, showScroll } from './manageScrollDocument';
import device from 'current-device';
import 'hammerjs';
import { debounce } from './helper';

// заполнение тегами свободного пространства в меню
function hideNotVisibleMenuTag(menuElement) {
	const menuWrap = menuElement.querySelector('.navigation-wrap')	
	const rectMenuWrap = menuWrap.getBoundingClientRect();
	const tags = menuElement.querySelectorAll('.navigation-tags li');

	if( tags.length ) {
		for (let indexTag in [...tags]) {
			const tag = tags[indexTag];
	
			tag.style.display = '';
	
			const rectTag = tag.getBoundingClientRect();

			if( rectTag.bottom > rectMenuWrap.bottom ) {
				const hiddenElements = menuElement.querySelectorAll(`.navigation-tags li:nth-of-type(n + ${+indexTag+1})`);
	
				for (const element of [...hiddenElements]) {
					element.style.display = 'none';
				}
				break;
			}
		}
	}
}

const debHideNotVisibleMenuTag = debounce(hideNotVisibleMenuTag, 400);

window.addEventListener('resize', () => { 
	if( document.getElementById('navigation').classList.contains('opened') ) {
		debHideNotVisibleMenuTag(document.getElementById('navigation')); 
	}
});

// открытие/закрытие меню
document.addEventListener('click', e => {
	if( !e.target.closest('.header .menu-toggle') ) return;

	const button = e.target.closest('.header .menu-toggle');
	const menu = document.getElementById('navigation');
	const isOpened = menu.classList.contains('opened');

	isOpened ? closeMenu(button, menu) : openMenu(button, menu);
}); 

// закрытие меню при смене ориентации 
device.onChangeOrientation(() => {
	if( document.getElementById('navigation').classList.contains('opened') ) {
		closeMenu(document.querySelector('.menu-toggle'), document.getElementById('navigation'));
	}
});

function createBackdropMobileMenu() {
	const backdropElement = document.createElement('div');

	backdropElement.id = 'backdrop-navigation';
	backdropElement.style.transition = '0.2s ease-in-out';
	backdropElement.style.position = 'fixed';
	backdropElement.style.top = 0;
	backdropElement.style.right = 0;
	backdropElement.style.bottom = 0;
	backdropElement.style.left = 0;
	backdropElement.style.background = 'var(--color-black)';
	backdropElement.style.opacity = 0;
	backdropElement.style.zIndex = 50;

	document.body.appendChild(backdropElement);

	setTimeout(() => {
		backdropElement.style.opacity = 0.8;
	}, 50)
}

function removeBackdropMobileMenu() {
	const backdropElement = document.getElementById('backdrop-navigation');

	backdropElement.style.opacity = 0;
	backdropElement.style.pointerEvents = 'none';

	setTimeout(() => {
		backdropElement.remove();
	}, 300);
}

// открытие меню
function openMenu(button, menuElement) {
	const swipe = new Hammer(document.body);

	menuElement.classList.add('opened');
	menuElement.style.display = 'block';

	hideNotVisibleMenuTag(menuElement);

	button.innerHTML = '<svg class="icon-svg-close" aria-label="Close menu"><use xlink:href="#close"></svg>';

	createBackdropMobileMenu();

	swipe.on('swipeleft', () => {
		closeMenu(button, menuElement);
	});

	menuElement.classList.add('animated', 'animateMenuOpen', 'fast');
	hideScroll();
}

// закрытие меню
function closeMenu(button, menuElement) {
	const swipe = new Hammer(document.body);

	menuElement.classList.remove('opened');
	menuElement.classList.remove('animateMenuOpen');
	menuElement.classList.add('animated', 'animateMenuClose', 'fast');

	button.innerHTML = '<svg class="icon-svg-menu-toggle" aria-lable="Open menu"><use xlink:href="#menu-toggle"></svg>';

	removeBackdropMobileMenu();

	swipe.off('swipeleft', () => {
		closeMenu(button, menuElement);
	});

	setTimeout(() => {
		menuElement.classList.remove('animated', 'animateMenuClose', 'fast');
		menuElement.style.display = 'none';
	}, 500);
	showScroll();
}
