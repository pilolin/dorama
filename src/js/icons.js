//	icon structure 
//	info: {
//		name: name
//		symbol: icon body - required
//		width: width
//		height: height
//	},

// цвета иконок необходимо заменить на переменные, для удобного управления цветом из css

const defaultWidth = 22;
const defaultHeight = 22;

const gradients = {
	linearGradientColor2: '<stop stop-color="var(--color-stop-1)"/><stop offset="1" stop-color="var(--color-stop-2)"/>'
}

const icons = {
	info: {
		symbol:`
			<circle cx="11" cy="11" r="11" fill="var(--color)"/>
			<path fill="var(--color-bg)" d="M10.891 8.39737C11.569 8.39737 12.0939 7.85059 12.0939 7.17258C12.0939 6.51644 11.5471 5.96966 10.891 5.96966C10.2348 5.96966 9.68803 6.51644 9.68803 7.17258C9.68803 7.85059 10.2348 8.39737 10.891 8.39737ZM12.1814 10.1033C12.1814 9.55656 11.7439 9.11913 11.1972 9.11913H10.2129C9.66616 9.11913 9.22873 9.55656 9.22873 10.1033C9.22873 10.6501 9.66616 11.0876 10.2129 11.0876V13.9527C9.66616 13.9527 9.22873 14.3901 9.22873 14.9369C9.22873 15.4837 9.66616 15.9211 10.2129 15.9211H12.1814C12.7281 15.9211 13.1656 15.4837 13.1656 14.9369C13.1656 14.3901 12.7281 13.9527 12.1814 13.9527V10.1033ZM11.0003 1.15796C16.4244 1.15796 20.8424 5.57597 20.8424 11.0001C20.8424 16.4242 16.4244 20.8422 11.0003 20.8422C5.55434 20.8422 1.1582 16.4242 1.1582 11.0001C1.1582 5.57597 5.55434 1.15796 11.0003 1.15796Z"/>`,
	},
	play: {
		symbol:`
			<circle cx="11" cy="11" r="11" fill="var(--color)"/>
			<circle cx="11.0001" cy="11.0001" r="9.9" fill="var(--color-bg)"/>
			<path fill="var(--color)" d="M8.7998 13.4619V8.4001C8.7998 7.61556 9.66199 7.13657 10.3281 7.55104L14.1245 9.91323C14.7308 10.2905 14.7577 11.1634 14.1757 11.5772L10.3793 14.2769C9.71725 14.7477 8.7998 14.2743 8.7998 13.4619Z"/>`,
	},
	search: {
		symbol:`
			<path d="M15.5 17.5L20.5 23" stroke="var(--color)" stroke-width="2" stroke-linecap="round"/>
			<circle cx="10" cy="10" r="9" stroke="var(--color)" fill="var(--color-bg)" stroke-width="2"/>
			<path d="M8.99007 4.849C8.23281 4.83024 7.05309 5.10194 6.03539 6.20388C5.01768 7.30582 4.84046 8.50338 4.91924 9.25676" stroke="var(--color-flare)" stroke-linecap="round" stroke-linejoin="round"/>`,
		width: 22,
		height: 24
	},
	sort: {
		symbol:`
			<path d="M1 10L4 7L7 10" stroke="var(--color)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M1 4L4 1L7 4" stroke="var(--color)" stroke-linecap="round" stroke-linejoin="round"/>`,
		width: 8,
		height: 11
	},
	link: {
		symbol:`
			<path d="M2 6.5L7 6.5V11.5" stroke="var(--color)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M5 3.5L10.0001 3.5L10.0001 8.5" stroke="var(--color-shadow)" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M6.5 7L2 11.5" stroke="var(--color)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>`,
		width: 14,
		height: 13
	},
	dropdown: {
		symbol:`
			<path d="M14.5 7.5L11 10.5L7.5 7.5" stroke="var(--color-top)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M14.5 11.5L11 14.5L7.5 11.5" stroke="var(--color-bottom)" stroke-linecap="round" stroke-linejoin="round"/>`,
	},
	close: {
		symbol:`
			<path d="M6.92128 14.0062L2.68904 18.2384C2.29852 18.629 2.29852 19.2621 2.68904 19.6527C3.07956 20.0432 3.71273 20.0432 4.10325 19.6527L8.33549 15.4204C8.72602 15.0299 8.72602 14.3967 8.33549 14.0062C7.94497 13.6157 7.3118 13.6157 6.92128 14.0062Z" fill="var(--color)"/>
			<path d="M18.3282 2.3112L13.6524 6.98699C13.2619 7.37751 13.2619 8.01067 13.6524 8.4012C14.0429 8.79172 14.6761 8.79172 15.0666 8.4012L19.7424 3.72541C20.1329 3.33489 20.1329 2.70172 19.7424 2.3112C19.3519 1.92068 18.7187 1.92067 18.3282 2.3112Z" fill="var(--color)"/>
			<path d="M19.6229 18.2748L3.67195 2.32381C3.28143 1.93328 2.64826 1.93328 2.25774 2.32381C1.86721 2.71433 1.86721 3.3475 2.25774 3.73802L18.2087 19.689C18.5992 20.0795 19.2324 20.0795 19.6229 19.689C20.0135 19.2985 20.0135 18.6653 19.6229 18.2748Z" fill="var(--color)"/>`,
	},
	random: {
		symbol:`
			<rect x="1" y="1" width="20" height="20" rx="3" stroke="var(--color)" fill="var(--color-bg)" stroke-width="2"/>
			<circle cx="7" cy="7" r="2" fill="var(--color)"/>
			<circle cx="15" cy="15" r="2" fill="var(--color)"/>`,
	},
	star: {
		symbol: `
			<path d="M11.4755 3.08156L12.7696 7.06434C12.9704 7.68237 13.5464 8.10081 14.1962 8.10081H18.3839C18.8683 8.10081 19.0697 8.72062 18.6778 9.00532L15.2899 11.4668C14.7641 11.8488 14.5442 12.5258 14.745 13.1439L16.0391 17.1266C16.1887 17.5873 15.6615 17.9704 15.2696 17.6857L11.8817 15.2242C11.3559 14.8422 10.6441 14.8422 10.1183 15.2242L6.73037 17.6857C6.33851 17.9704 5.81127 17.5873 5.96095 17.1266L7.25503 13.1439C7.45584 12.5258 7.23585 11.8488 6.71012 11.4668L3.32217 9.00532C2.93031 8.72062 3.1317 8.10081 3.61606 8.10081H7.8038C8.45364 8.10081 9.02958 7.68237 9.23039 7.06434L10.5245 3.08156C10.6741 2.6209 11.3259 2.6209 11.4755 3.08156Z" stroke="var(--color-border)"/>
			<path d="M10.0489 2.92705C10.3483 2.00574 11.6517 2.00574 11.9511 2.92705L13.2451 6.90983C13.379 7.32185 13.763 7.60081 14.1962 7.60081H18.3839C19.3527 7.60081 19.7554 8.84043 18.9717 9.40983L15.5838 11.8713C15.2333 12.126 15.0866 12.5773 15.2205 12.9894L16.5146 16.9721C16.8139 17.8934 15.7595 18.6596 14.9757 18.0902L11.5878 15.6287C11.2373 15.374 10.7627 15.374 10.4122 15.6287L7.02426 18.0902C6.24054 18.6596 5.18607 17.8934 5.48542 16.9721L6.7795 12.9894C6.91338 12.5773 6.76672 12.126 6.41623 11.8713L3.02827 9.40983C2.24456 8.84043 2.64734 7.60081 3.61606 7.60081H7.8038C8.23703 7.60081 8.62099 7.32185 8.75486 6.90983L10.0489 2.92705Z" fill="var(--color-fill)"/>`,
	},
	menuToggle: {
		symbol: `
			<rect y="5.5" width="22" height="1.83333" rx="0.916667" fill="var(--color)"/>
			<rect y="14.6667" width="12.8333" height="1.83333" rx="0.916667" fill="var(--color)"/>
			<rect x="15.5835" y="14.6667" width="6.41667" height="1.83333" rx="0.916667" fill="var(--color)"/>` 
	},
	arrowNext: {
		symbol: `
			<path d="M5 16L10 11L5 6" stroke="var(--color)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M12 16L17 11L12 6" stroke="var(--color)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>`
	},
	arrowPrev: {
		symbol: `
			<path d="M17 16L12 11L17 6" stroke="var(--color)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M10 16L5 11L10 6" stroke="var(--color)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>`
	},
	chevronLeft: {
		symbol: `<path d="M17 1L5.33333 11L17 21" stroke="var(--color)" stroke-width="2"/>`
	},
	chevronRight: {
		symbol: `<path d="M5 1L16.6667 11L5 21" stroke="var(--color)" stroke-width="2"/>`
	},
	playTriangle: {
		symbol: `<path d="M5 21.0132V3.66378C5 2.08136 6.75068 1.12569 8.08169 1.98154L20.5732 10.0137C21.7476 10.7688 21.8065 12.4643 20.6873 13.2991L8.19577 22.6163C6.87664 23.6002 5 22.6588 5 21.0132Z" fill="var(--color)"/>`,
		width: 25,
		height: 25
	}
}

let strIcons = `<svg aria-hidden="true" focusable="false" style="width:0;height:0;position:absolute;">`;

for (const key in icons) {
	const icon = icons[key];
	const name = icon.name ? icon.name : camelToKebabCase(key);
	const width = icon.width ? icon.width : defaultWidth;
	const height = icon.height ? icon.height : defaultHeight;

	strIcons += `<symbol id="${name}" fill="none" viewBox="0 0 ${width} ${height}">
								${icon.symbol}
							</symbol>`;
}

for (const name in gradients) {
	strIcons += `<linearGradient id="${camelToKebabCase(name)}">
								${gradients[name]}
							</linearGradient>`;
}

strIcons += `</svg>`;

const svgContainer = document.createElement('div');

svgContainer.innerHTML = strIcons;
document.body.appendChild(svgContainer);

function camelToKebabCase(str) {
	return str.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();
}