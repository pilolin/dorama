import './icons';
import { Swiper, Navigation } from 'swiper/js/swiper.esm.js';
import 'hammerjs';
import 'simplebar';
import Select from './select';
import 'noUiSlider';
import CustomSlider from './custom-slider';
import './filter';
import SnowMoreText from './show-more';
import 'current-device';
import './serial-info-popover';
import webkitLineClamp from 'webkit-line-clamp';
import { isTouchDevice } from './helper';
import './menu';
import './vote';
import './header-search';
import './smoothScroll';
import './novelty';
import './modal';
import './../scss/styles.scss'

Swiper.use([Swiper, Navigation]);

new Select('.select');

new SnowMoreText('.serial-description-text');

for (const description of [...document.querySelectorAll('[data-webkit-line-clamp]')]) {
	webkitLineClamp(description, +description.dataset.webkitLineClamp);
}

document.addEventListener('DOMContentLoaded', () => {
	if(isTouchDevice()) {
		document.body.classList.add('touch');
	} else {
		document.body.classList.add('no-touch');
	}
});

// табы
document.addEventListener('click', e => {
	const target = e.target;

	if( !target.closest('[role="tab"]') || target.closest('.tabs[data-custom-toggle]') ) return 0;

	const tabsContainer = target.closest('.tabs');
	const clickedTab = target.closest('.tabs-list__item');
	const contentForView = tabsContainer.querySelector(clickedTab.firstElementChild.hash);

	tabsContainer.querySelector('.tabs-list__item_active').classList.remove('tabs-list__item_active');
	tabsContainer.querySelector('.tabs-content-wrap_active').classList.remove('tabs-content-wrap_active');

	clickedTab.classList.add('tabs-list__item_active');
	contentForView.classList.add('tabs-content-wrap_active');

	e.preventDefault();
});

// слайдеры связанных и похожих дорам
window.addEventListener('load', () => {
	new Swiper('.related-serials .catalog-slider, .similar-serials .catalog-slider', {
		slidesPerView: 2.4,
		spaceBetween: 12,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			577: {
				slidesPerView: 2.4,
				spaceBetween: 15
			},
			769: {
				slidesPerView: 3.4,
				spaceBetween: 15
			},
			993: {
				slidesPerView: 4.4,
				spaceBetween: 15
			},
			1025: {
				slidesPerView: 5,
				spaceBetween: 20,
			}
		}
	});
});

// сворачивание/разврачиванеи расписания
document.addEventListener('click', e => {
	if( !e.target.closest('.schedule .schedule__show-more') ) return 0;

	const schedule = e.target.closest('.schedule');
	const btn = e.target.closest('.schedule__show-more');
	const scheduleList = schedule.querySelector('.schedule-list');
	const opened = (scheduleList.getAttribute('aria-expanded') === 'true');

	if( !opened ) {
		scheduleList.setAttribute('aria-expanded', true);
		btn.dataset.textForHiddenBlock = btn.querySelector('span').innerText;
		btn.querySelector('span').innerText = 'Свернуть';
	} else {
		scheduleList.setAttribute('aria-expanded', false);
		btn.querySelector('span').innerText = btn.dataset.textForHiddenBlock;
	}

	e.preventDefault();
});

// слайдер кадров сериала
window.addEventListener('load', () => {
	new CustomSlider('#frames-slider').init();
});

// слайдер списка серий на странице эпизода
window.addEventListener('load', () => {
	new Swiper('.all-episodes-slider .swiper-container', {
		slidesPerView: 2.5,
		spaceBetween: 12,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			769: {
				slidesPerView: 3.4,
				spaceBetween: 12,
			},
			993: {
				slidesPerView: 4.4,
				spaceBetween: 16,
			},
			1025: {
				slidesPerView: 4.4,
				spaceBetween: 20,
			}
		}
	});
});

// Табы озвучек эпизода
window.addEventListener('load', initEpisodeSoundsSlider);

function initEpisodeSoundsSlider() {
	const episodeSoundsSlider = new Swiper('.episode-sounds .tabs-list', {
		slidesPerView: 'auto',
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			993: {
				slidesPerView: 3
			},
			1025: {
				slidesPerView: 4
			}
		}
	});

	document.addEventListener('click', e => {
		if( !e.target.closest('.episode-sounds .tabs-list__item') ) return;
	
		const tabsList = document.querySelectorAll('.episode-sounds .tabs-list .tabs-list__item');
		const targetTab = e.target.closest('.tabs-list__item');
		const activeTab = e.target.closest('.episode-sounds').querySelector('.tabs-list__item_active');
	
		activeTab.classList.remove('tabs-list__item_active');
		targetTab.classList.add('tabs-list__item_active');
	
		episodeSoundsSlider.slideTo([...tabsList].indexOf(targetTab));
	
		e.preventDefault();
	});
}

// Очистка поля поиска 
document.addEventListener('click', e => {
	if( !e.target.closest('.search-clean') ) return;

	const input = e.target.closest('.search-clean').previousElementSibling;

	input.value = '';
	input.focus();
})