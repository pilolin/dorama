export function debounce(f, ms) {
	let isCooldown = false;

  return function() {
    if (isCooldown) return;
    f.apply(this, arguments);
    isCooldown = true;
    setTimeout(() => isCooldown = false, ms);
  };
}

export function сutString(str, maxLength) {
	return str.split(' ').reduce((res, cur) => ((res + ' ' + cur).length <= maxLength ? (res + ' ' + cur) : res)) + '...';
}

export function isTouchDevice() {
	return 'ontouchstart' in window;
}