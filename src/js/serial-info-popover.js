import device from 'current-device';
import { hideScroll, showScroll } from './manageScrollDocument';
import { сutString } from './helper';

const serialProps = {
	ratings: {
		kinopoisk: 'КиноПоиск',
		imdb: 'IMDb',
		myDramaList: 'MyDramaList',
	},
	genre: 'Жанры',
	sounds: 'Озвучка'
};

const desktopPopoverWidth = 445;

const mockSettingSerial = {
	title: '«Цветочная команда: Брачное агентство Чосона»',
	alternativeTitle: 'Агентство по организации браков в Чосоне / Агентство по организации браков в Чосоне',
	ratings: { kinopoisk: '7.9', imdb: '7.9', myDramaList: '7.9' },
	genre: 'драмы, мелодрама',
	sounds: 'рус. субтитры',
	status: 'Выходит',
	description: 'Если вам когда-нибудь вдруг станет любопытно, с чего начинается настоящая любовь — спросите Клэр Дуруг станет любопытно, с чего начинается настоящая любовь — спросите Клэр Дуруг станет любопытно, с чего начинается настоящая любовь — спросите Клэр Дуруг станет любопытно, с чего начинается настоящая любовь — спросите Клэр Дуруг станет любопытно, с чего начинается настоящая любовь — спросите Клэр Дункан. Она не любовь — спросите Клэр Дункан. Она не задумываюсь вам ответит: «С первого поцелуя!»',
	trailer: 'https://www.youtube.com/embed/dQw4w9WgXcQ',
	firstEpisode: 'javascript:void(0);',
	aboutSerialLink: 'javascript:void(0);'
}

/* 
 * new SerialInfoPopover().init(element, {
 *		title,
 *		alternativeTitle,
 *		ratings: {
 *			kinopoisk,
 *			imdb,
 *			myDramaList
 *		},
 *		status,
 *		genre,
 *		sounds,
 *		description,
 *		trailer,
 *		firstEpisode,
 *		aboutSerialLink
 * })
 */

class SerialInfoPopover {
	constructor() {
		this.element;
		this.serialData;
		this.popover;
		this.popoverType;
		this.backdropElement;
	}

	init(element, data) {
		const documentWidth = document.documentElement.clientWidth;

		this.element = element;
		this.serialData = !data || argv.mode === 'development' ? mockSettingSerial : data;

		if( device.desktop() && ((documentWidth - this.element.clientWidth) / 2) > desktopPopoverWidth ) {
			this.popoverType = 'desktop';
		} else {
			this.popoverType = 'mobile';
		}

		this.createPopover();

		this.open(this.popover);
	}

	createPopover() {
		this.popover = document.createElement('div');
		this.popover.setAttribute('id', 'serial-' + this.element.dataset.serialId);
		this.popover.classList.add('serial-info-popover');

		if(this.popoverType === 'desktop') { // desktop
			this.createDesktopPopover();
		} else { // mobile
			this.createMobilePopover();
		}
	}

	createDesktopPopover() {
		let serialPropsList = '';

		this.popover.dataset.type = 'desktop';

		if(this.serialData.ratings.myDramaList || this.serialData.genre || this.serialData.sounds) {
			serialPropsList += '<ul>';
			serialPropsList += this.serialData.ratings.myDramaList ? `<li><span>${serialProps.ratings.myDramaList}</span><span>${this.serialData.ratings.myDramaList}</span></li>` : '';
			serialPropsList += this.serialData.genre ? `<li><span>${serialProps.genre}</span><span>${this.serialData.genre}</span></li>` : '';
			serialPropsList += this.serialData.sounds ? `<li><span>${serialProps.sounds}</span><span>${this.serialData.sounds}</span></li>` : '';
			serialPropsList += '</ul>';
		}

		this.popover.innerHTML = `
			<div class="serial-info-popover__arrow"></div>
			<div class="serial-info-popover-content">
				<div class="serial-info-popover-wrap">
					<div class="serial-info-popover__header">
						<h3>${this.serialData.title}</h3>
						<span>${this.serialData.alternativeTitle}</span>
					</div>
					<div class="serial-info-popover-body">
						<div class="serial-info-popover__props">${serialPropsList}</div>
						<div class="serial-info-popover-description">
							${сutString(this.serialData.description, 195)}
							<a href="${this.serialData.aboutSerialLink}" class="external-link-arrow"><svg class="icon-svg-link"><use xlink:href="#link"></svg></a>
						</div>
					</div>
					<div class="serial-info-popover-footer">
						<button class="button button_type_border" data-modal-open-iframe="${this.serialData.trailer}"><span>Трейлер</span></button>
						<a href="${this.serialData.firstEpisode}" class="button"><span>1 серия</span></a>
						</div>
					</div>
				</div>`;

			document.body.appendChild(this.popover);

			window.addEventListener('resize', () => { this.setPositionDesktop() });
			window.addEventListener('scroll', () => { this.setPositionDesktop() });
	}

	createMobilePopover() {
		let serialPropsList = '';

		this.popover.dataset.type = 'mobile';

		if(Object.keys(this.serialData.ratings).length) {
			serialPropsList += '<ul class="serial-info-popover__ratings">';
			for (const ratingKey in this.serialData.ratings) {
				serialPropsList += `<li><span>${serialProps.ratings[ratingKey]}</span><span>${this.serialData.ratings[ratingKey]}</span></li>`
			}
			serialPropsList += '</ul>';
		}

		if(this.serialData.genre) {
			serialPropsList += `<div class="serial-info-popover__genre"><span>${serialProps.genre}</span><span>${this.serialData.genre}</span></div>`;
		}

		if(this.serialData.sounds) {
			serialPropsList += `<div class="serial-info-popover__sounds"><span>${serialProps.sounds}</span><span>${this.serialData.sounds}</span></div>`;
		}

		this.popover.innerHTML = `
			<div class="serial-info-popover-content">
				<div class="serial-info-popover-wrap">
					<button class="serial-info-popover__close"><svg class="icon-svg-close"><use xlink:href="#close"></svg></button>
					<div class="serial-info-popover__header">
						<h3>${this.serialData.title}</h3>
						<span>${this.serialData.alternativeTitle}</span>
					</div>
					<div class="serial-info-popover-body">
						<div class="serial-info-popover__trailer">
							<iframe src="${this.serialData.trailer}" frameborder="0"></iframe>
							<span class="serial-info-popover__serial-status">${this.serialData.status}</span>
						</div>
						<div class="serial-info-popover__props">${serialPropsList}</div>
						<div class="serial-info-popover-description">
							<h4>Описание</h4>
							<div class="serial-info-popover-description__content" data-simplebar data-simplebar-auto-hide="false">
								${сutString(this.serialData.description, 195)}
							</div>
						</div>
					</div>
					<div class="serial-info-popover-footer">
						<a href="${this.serialData.aboutSerialLink}" class="button button_type_border"><span>о дораме</span></a>
						<a href="${this.serialData.firstEpisode}" class="button"><span>1 серия</span></a>
					</div>
				</div>
			</div>`;

		document.body.appendChild(this.popover);

		window.addEventListener('resize', () => { this.setHeightDescriptionMobile(); });

		this.popover.querySelector('.serial-info-popover__close').addEventListener('click', () => { this.close(this.popover); });
	}

	createBackdrop() {
		this.backdropElement = document.createElement('div');

		this.backdropElement.id = 'serial-info-popover-backdrop';
		this.backdropElement.style.transition = '0.2s ease-in-out';
		this.backdropElement.style.position = 'fixed';
		this.backdropElement.style.top = 0;
		this.backdropElement.style.right = 0;
		this.backdropElement.style.bottom = 0;
		this.backdropElement.style.left = 0;
		this.backdropElement.style.background = 'var(--color-black)';
		this.backdropElement.style.opacity = 0;
		this.backdropElement.style.zIndex = 550;

		document.body.appendChild(this.backdropElement);

		this.popover.addEventListener('click', e => { this.closeClickWithoutModal(e); });

		setTimeout(() => {
			this.backdropElement.style.opacity = 0.8;
		}, 50)
	}

	removeBackdrop() {
		this.backdropElement = document.getElementById('serial-info-popover-backdrop');

		this.popover.removeEventListener('click', e => { this.closeClickWithoutModal(e); } );
		
		if( this.backdropElement ) {
			this.backdropElement.style.opacity = 0;
			this.backdropElement.style.pointerEvents = 'none';
	
			setTimeout(() => {
				this.backdropElement.remove();
			}, 300);
		}
	}

	setHeightDescriptionMobile() {
		if(this.popoverType === 'mobile') {
			const description = this.popover.querySelector('.serial-info-popover-description__content');
			const positionDescription = description.getBoundingClientRect();
			const positionPopoverWrap = this.popover.querySelector('.serial-info-popover-wrap').getBoundingClientRect();

			description.style.height = positionPopoverWrap.bottom - positionDescription.top - 5 + 'px';
		}
	}

	setPositionDesktop() {
		const positionElement = this.element.getBoundingClientRect();
		const documentWidth = document.documentElement.clientWidth;
		
		this.popover.style.top = positionElement.top - 25 + 'px';

		if( (documentWidth - positionElement.right - desktopPopoverWidth) > 0 ) {
			this.popover.style.left = positionElement.right + 'px';
			this.popover.style.paddingLeft = '18px';
			this.popover.dataset.position = 'right';
		} else {
			this.popover.style.left = positionElement.left - desktopPopoverWidth + 'px';
			this.popover.style.paddingRight = '18px';
			this.popover.dataset.position = 'left';
		}
	}

	open(popoverElement) {
		popoverElement.setAttribute('aria-hidden', false);
		popoverElement.style.display = 'block';

		if(popoverElement.dataset.type === 'desktop') { // desktop
			this.setPositionDesktop();
			popoverElement.classList.add('animated', 'fadeIn', 'fast');
		} else { // mobile
			this.createBackdrop();
			hideScroll();
			this.setHeightDescriptionMobile();
			popoverElement.classList.add('animated', 'fadeInUp', 'fast');
		}
	}
	
	close(popoverElement) {
		if(!popoverElement) return;

		popoverElement.classList.remove('fadeIn', 'fadeInUp');
		popoverElement.setAttribute('aria-hidden', true);

		if(popoverElement.dataset.type === 'desktop') { // desktop
			popoverElement.classList.add('animated', 'fadeOut', 'fast');
		} else { // mobile
			this.removeBackdrop();
			showScroll();
			popoverElement.classList.add('animated', 'fadeOutDown', 'fast');
		}

		this.element.classList.remove('popover-opened');

		setTimeout(() => {
			popoverElement.classList.remove('animated', 'fadeOut', 'fadeOutDown', 'fast');
			popoverElement.style.display = 'none';
		}, 500);
	}

	closeClickWithoutModal(event) {
		if( event.target.closest('.serial-info-popover-content') ) return;

		this.close(document.querySelector(`.serial-info-popover[aria-hidden="false"][data-type="mobile"]`));
	}
}

window.SerialInfoPopover = SerialInfoPopover;

// вызовы инфо сериалов
const SerialInfoPopoverInstance = new SerialInfoPopover();

document.addEventListener('click', e => {
	if( !(e.target.closest('.catalog-item__icon-info') && ('ontouchstart' in window || window.matchMedia('(max-width: 992px)').matches)) ) return;
	
	const targetIcon = e.target.closest('.catalog-item__icon-info');
	const popover = document.querySelector(`#serial-${targetIcon.dataset.serialId}.serial-info-popover[data-type="mobile"]`);

	if( popover && popover.getAttribute('aria-hidden') === 'false' ) return;

	if( targetIcon.classList.contains('info-loaded') ) {
		SerialInfoPopoverInstance.open(document.querySelector(`#serial-${targetIcon.dataset.serialId}.serial-info-popover[data-type="mobile"]`))
	} else {

		/*
			get data for serial
		*/

		SerialInfoPopoverInstance.init(targetIcon);
		targetIcon.classList.add('info-loaded');
	}

	targetIcon.classList.add('popover-opened');

	e.preventDefault();
});

const buttonsForOpenPopoverInfo = document.querySelectorAll('.catalog-item__icon-info');

for (const button of [...buttonsForOpenPopoverInfo]) {
	button.addEventListener('mouseenter', e => {
		if( ('ontouchstart' in window) || window.matchMedia('(max-width: 992px)').matches ) return;
		
		const popover = document.querySelector(`#serial-${button.dataset.serialId}.serial-info-popover[data-type="desktop"]`);
	
		if( popover && popover.getAttribute('aria-hidden') === 'false' ) return;
	
		if( button.classList.contains('info-loaded') ) {
			SerialInfoPopoverInstance.open(document.querySelector(`#serial-${button.dataset.serialId}.serial-info-popover[data-type="desktop"]`))
		} else {
	
			/*
				get data for serial
			*/
	
			SerialInfoPopoverInstance.init(button);
			button.classList.add('info-loaded');
		}

		button.classList.add('popover-opened');

		
		e.preventDefault();
	});
}

document.addEventListener('mousemove', e => {
	if( ('ontouchstart' in window) || window.matchMedia('(max-width: 992px)').matches ) return;

	if( e.target.closest('.catalog-item__icon-info') || e.target.closest('.serial-info-popover') ) return;

	const popover = document.querySelector(`.serial-info-popover[aria-hidden="false"][data-type="desktop"]`);
	
	if( !popover ) return;
                       
	const button = document.querySelector(`.catalog-item__icon-info[data-serial-id="${popover.id.replace(/[^\d]/g, '')}"]`);

	SerialInfoPopoverInstance.close(popover);

	button.classList.remove('popover-opened');
});