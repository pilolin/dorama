import { Swiper, Navigation } from 'swiper/js/swiper.esm.js';

Swiper.use([Swiper, Navigation]);

let activeSlider;

document.addEventListener('click', e => {
	const target = e.target;

	if( !(target.closest('[role="tab"]') && target.closest('.novelty__tabs')) ) return 0;

	const tabsContainer = target.closest('.tabs');
	const clickedTab = target.closest('.tabs-list__item');
	const contentForView = tabsContainer.querySelector(clickedTab.firstElementChild.hash);

	tabsContainer.querySelector('.tabs-list__item_active').classList.remove('tabs-list__item_active');
	tabsContainer.querySelector('.tabs-content-wrap_active').classList.remove('tabs-content-wrap_active');

	clickedTab.classList.add('tabs-list__item_active');
	contentForView.classList.add('tabs-content-wrap_active');

	activeSlider.destroy();
	initSliderActiveTab();

	e.preventDefault();
});

function initSliderActiveTab() {
	activeSlider = new Swiper('.novelty .tabs-content-wrap_active .catalog-slider', {
		slidesPerView: 2.4,
		spaceBetween: 12,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			576: {
				slidesPerView: 3.4,
				spaceBetween: 12
			},
			768: {
				slidesPerView: 4.4,
				spaceBetween: 15
			},
			1025: {
				slidesPerView: 5,
				spaceBetween: 20,
			}
		}
	});
}

window.addEventListener('load', initSliderActiveTab);