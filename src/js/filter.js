import noUiSlider from 'noUiSlider';

function filter() {
	// Даты выхода сериалов
	const filter = document.getElementById('filter');
	let rangeDate;

	if( filter ) {
		const inputRangeDateMin = document.querySelector('#filter-date-release-min');
		const inputRangeDateMax = document.querySelector('#filter-date-release-max');

		rangeDate = filter.querySelector('#field-date-release');
	
		noUiSlider.create(rangeDate, {
				start: [+rangeDate.dataset.startLeft, +rangeDate.dataset.startRight],
				connect: true,
				range: {
					min: +rangeDate.dataset.min,
					max: +rangeDate.dataset.max,
				},
				step: 1,
				limit: rangeDate.dataset.limit ? +rangeDate.dataset.limit : 25,
				tooltips: true,
				format: {
					to: value => value,
					from: value => +value
				},
				pips: {
					mode: 'steps',
					stepped: true,
					density: 4
			}
		});
		
		// обновление полей input даты при обновлении слайдера
		rangeDate.noUiSlider.on('update', (values, handle) => {
			if (handle) {
				inputRangeDateMax.value = values[handle];
			} else {
				inputRangeDateMin.value = values[handle];
			}
		});

		// задание значения слайдеру при изменении полей дат
		inputRangeDateMin.addEventListener('input', e => {
			rangeDate.noUiSlider.set([e.target.value, null]);
		});

		inputRangeDateMax.addEventListener('input', e => {
			rangeDate.noUiSlider.set([null, e.target.value]);
		});

		// сброс фильтра
		document.addEventListener('click', e => {
			if( !e.target.closest('#button-reset-filter') ) return;
		
			const filterForm = document.querySelector('#filter form');
			const selectElements = filterForm.querySelectorAll('select');
		
			filterForm.reset();
			
			for (const select of [...selectElements]) {
				select.dispatchEvent(new Event('change'));
			}
		
			rangeDate.noUiSlider.reset()
		});

		function setFilterSettings(args) {
			const options = {
				'date-release-min': args.dateReleaseMin || rangeDate.dataset.min,
				'date-release-max': (args.dateReleaseMax && args.dateReleaseMax > args.dateReleaseMin) ? args.dateReleaseMax : rangeDate.dataset.max,
				country						: args.country || '',
				status						: args.status || '',
				genre							: args.genre || '',
				sound							: args.sound || '',
				'date-release'		: args.dateReleaseMin || ''
			};

			const filterForm = document.querySelector('#filter form');

			for (const name of Object.keys(options)) {
				const field = filterForm.querySelector(`[name="${name}"]`);

				field.value = options[name];

				field.dispatchEvent(new Event(field.tagName.toLowerCase() === 'input' ? 'input' : 'change'));
			}
		}

		window.setFilterSettings = setFilterSettings;
	}
}

// инициализация фильтра
document.addEventListener('DOMContentLoaded', filter);