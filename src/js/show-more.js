import device from 'current-device';

export default class SnowMoreText {
	constructor(selector) {
		this.element = document.querySelector(selector);

		if( !this.element ) return;

		this.container = this.element.querySelector('.show-more-wrap');
		this.toggleBtn = this.element.querySelector('.show-more__toggle');

		this.height = eval(this.container.dataset.height);

		this.init();

		document.addEventListener('click', (e) => { this.toggle(e); });
	}

	init() {
		this.container.setAttribute('aria-expanded', false);
		if( device.desktop() ) {
			this.container.style.maxHeight = this.height.desktop + 'px';
		} else {
			this.container.style.maxHeight = (this.height.mobile ? this.height.mobile : this.height.desktop) + 'px';
		}
	}

	toggle(e) {
		if( e.target.closest('.show-more__toggle') != this.toggleBtn ) return;

		const opened = (this.container.getAttribute('aria-expanded') == 'true');

		if( !opened ) {
			this.container.setAttribute('aria-expanded', true);
			this.container.style.maxHeight = this.container.scrollHeight + 'px';
			this.toggleBtn.dataset.textForHiddenBlock = this.toggleBtn.querySelector('span').innerText;
			this.toggleBtn.querySelector('span').innerText = 'Свернуть';
		} else {
			this.container.setAttribute('aria-expanded', false);
			this.toggleBtn.querySelector('span').innerText = this.toggleBtn.dataset.textForHiddenBlock;

			if( device.desktop() ) {
				this.container.style.maxHeight = this.height.desktop + 'px';
			} else {
				this.container.style.maxHeight = (this.height.mobile ? this.height.mobile : this.height.desktop) + 'px';
			}
		}
	}
}