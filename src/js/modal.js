import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Swiper, Navigation } from 'swiper/js/swiper.esm.js';

Swiper.use([Swiper, Navigation]);

const swalModal = Swal.mixin({
	showConfirmButton: false,
	showCloseButton: true,
	closeButtonHtml: '<svg class="icon-svg-close" aria-label="Close modal" data-modal-close><use xlink:href="#close"></svg>',
	showClass: {
		popup: 'animated fadeInUp fast'
	},
	hideClass: {
		popup: 'animated fadeOutUp fast'
	}
});


// открытие модального окна по id
document.addEventListener('click', (e) => {
	const target = e.target;

	if( !(target.closest('a, button') && target.closest('a, button').dataset.modalOpen) ) return;

	const modalId = target.closest('a, button').dataset.modalOpen;

	swalModal.fire({
		html: document.getElementById(modalId).innerHTML,
		onOpen: (currentModal) => {
			const offsetTop = (window.innerHeight - currentModal.clientHeight) / 2;
			const buttonClose = currentModal.querySelector('.swal2-close');

			buttonClose.style.top = -(offsetTop / 2) + 'px';
		}
	});

	e.preventDefault();
});

// открытие в модальном окне ссылки с фреймом
document.addEventListener('click', (e) => {
	const target = e.target;

	if( !(target.closest('a, button') && target.closest('a, button').dataset.modalOpenIframe) ) return;

	const link = target.closest('a, button').dataset.modalOpenIframe;

	swalModal.fire({
		html: `<div class="modal-container modal_type_iframe">
						<div class="modal-content">
							<iframe src="${link}" frameborder="0" width="100%" height="100%"></iframe>
						</div>
					</div>`,
		onOpen: (currentModal) => {
			const offsetTop = (window.innerHeight - currentModal.clientHeight) / 2;
			const buttonClose = currentModal.querySelector('.swal2-close');

			buttonClose.style.top = -(offsetTop / 2) + 'px';
		}
	});

	e.preventDefault();
});

//галерея увеличенная
document.addEventListener('click', (e) => {
	const target = e.target;

	if( !((target.closest('.custom-slider__slide-active') && window.matchMedia('(min-width: 768px)').matches) ||
			(target.closest('.custom-slider__slide') && window.matchMedia('(max-width: 769px)').matches)) ) return;

	const slider = target.closest('.custom-slider-container');
	const slides = slider.querySelectorAll('.custom-slider__slide');
	const modal = document.getElementById('modal-gallery');
	const modalWrap = modal.querySelector('.swiper-wrapper');
	const realCountSlides = slides.length / 2;
	let modalSlides = '';

	for (let i = 2; i < realCountSlides + 2; i++) {
		const img = slides[i].querySelector('img').dataset.original;

		modalSlides += `<div class="swiper-slide" style="background-image: url(${img})"></div>`;
	}

	modalWrap.innerHTML = modalSlides;

	swalModal.fire({
		html: modal.innerHTML,
		customClass: {
			popup: 'swal2-gallery'
		},
		onOpen: (currentModal) => {
			new Swiper('.swal2-container .swiper-container', {
				loop: true,
				effect: 'fade',
				fadeEffect: {
					crossFade: true
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
		}
	});
});