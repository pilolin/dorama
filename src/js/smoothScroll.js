import smoothscroll from 'smoothscroll-polyfill';

smoothscroll.polyfill();

const delta = 15; // отступ от хедера (px)

function updateUrlHash(hash) {
	history.pushState("", document.title, window.location.pathname+(hash ? hash : ''));
}

function scrollPageToElement(idElement) {
	const heightHeader = document.getElementById('header').clientHeight;
	const scroll = document.querySelector(idElement).getBoundingClientRect().top + window.scrollY - heightHeader - delta;

	updateUrlHash(idElement);

	setTimeout(() => {
		window.scrollTo({top: scroll, behavior: 'smooth'});
	}, 100);
}

document.addEventListener('click', e => {
	const link = e.target.closest('a');

	if( !link ) return 0;

	const linkHash = link.getAttribute('href').replace(/^(\/)/, '');

	if(linkHash[0] === '#' && linkHash.length > 1 && link.parentElement.getAttribute('role') !== 'tab') {

		if( link.getAttribute('href')[0] === '/' && link.getAttribute('href').indexOf(window.location.pathname) === -1 ) return 0;

		scrollPageToElement(linkHash);

		e.preventDefault();
	}
});

window.addEventListener('load', () => {
	if(window.location.hash.length > 1) {
		scrollPageToElement(window.location.hash);
	}
})