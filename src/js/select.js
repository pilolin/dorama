import device from 'current-device';
export default class Select {
	constructor(selector) {
		this.selector = selector;

		this.items = document.querySelectorAll(this.selector);

		if( !this.items.length ) return;

		for (const item of [...this.items]) {
			item.addEventListener('click', e => { this.toggle(e) });
	
			item.querySelector('select').addEventListener('change', e => { this.changeSelect(e.target) });
	
			item.addEventListener('click', e => { this.selectOptionDropdown(e) });
		}
	}

	open(e) {
		if( !e.target.closest(`${this.selector} .select-field`)) return;

		if(device.desktop()) {
			const targetSelect = e.target.closest(this.selector);
			const opened = (targetSelect.getAttribute('aria-expanded') == 'true');
	
			this.closeAll();
	
			if( !opened ) {
				targetSelect.setAttribute('aria-expanded', true);
			}
		}
	}

	close(e) {
		if(device.desktop()) {
			const targetSelect = e.target.closest(this.selector);
			const opened = (targetSelect.getAttribute('aria-expanded') == 'true');

			if( opened ) {
				targetSelect.setAttribute('aria-expanded', false);
			}
		}
	}

	closeAll() {
		if(device.desktop()) {
			const openedSelect = document.querySelectorAll(`${this.selector}[aria-expanded="true"]`);

			if( openedSelect.length ) {
				for (const select of openedSelect) {
					select.setAttribute('aria-expanded', false);
				}
			}
		}
	}

	toggle(e) {
		if( !e.target.closest(`${this.selector} .select-field`)) return;

		if(device.desktop()) {
			const targetSelect = e.target.closest(this.selector);
			const opened = (targetSelect.getAttribute('aria-expanded') == 'true');

			this.closeAll();

			if( opened ) {
				targetSelect.setAttribute('aria-expanded', false);
			} else {
				targetSelect.setAttribute('aria-expanded', true);
			}
		}
	}

	selectOptionDropdown(e) {
		if( !e.target.closest(`${this.selector} li`)) return;

		const targetSelect = e.target.closest(this.selector);
		const selectElement = targetSelect.querySelector('select');

		selectElement.value = e.target.dataset.value;

		this.changeSelect(selectElement) 

		this.close(e);
	}

	changeSelect(selectElement) {
		if( selectElement.tagName.toLowerCase() !== 'select') return;
		
		const targetSelect = selectElement.closest(this.selector);
		const selectField = targetSelect.querySelector('.select-field__value');
		const selectedItem = targetSelect.querySelector('li.selected')
		const targetItem = targetSelect.querySelector(`li[data-value="${selectElement.value}"]`)

		selectField.innerText = selectElement.value ? selectElement.querySelector(`option[value="${selectElement.value}"]`).innerText : '';
		targetSelect.setAttribute('aria-selected', selectElement.value ? true : false);

		if(selectedItem) {
			selectedItem.classList.remove('selected');
		}

		if(targetItem) {
			targetItem.classList.add('selected');
		}
	}
}